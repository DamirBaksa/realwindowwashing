﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RagMotion : MonoBehaviour {

    private Window usingWindow;
    public Transform rotPos;
    public Slider dirtMeter;
    public Renderer ragRenderer;
    public GameObject currentWindow;

    Vector3 startPos;

    public float maxDirtTime = 10;
    public float dirtTime;
    public float rotateTime;
    public float rotWidth;
    public float rotHeight;
    public float rotSpeed;

    public bool isCleaning = false;

    public float cleanTimer;

    // Use this for initialization
    void Start ()
    {
        ragRenderer = GetComponent<Renderer>();
        dirtMeter.value = DirtMeterCalculation();

        Vector3 startPos = rotPos.position;
        rotWidth = 0.2f;
        rotHeight = 0.2f;
        rotSpeed = 7;
	}
	
	// Update is called once per frame
	void Update ()
    {
        usingWindow = currentWindow.GetComponent<Window>();
        transform.position = rotPos.position;
        transform.rotation = rotPos.rotation;
        if (usingWindow.isCleaning)
        {
            CircMotion();
            dirtTime += Time.deltaTime;
        }
        else
        {
            dirtTime += 0;
        }
        dirtMeter.value = DirtMeterCalculation();
        CleanRag();
    }

    public void CircMotion()
    {
        rotateTime += Time.deltaTime * rotSpeed;

        float x = rotPos.position.x + (Mathf.Cos(rotateTime) * rotWidth);
        float y = rotPos.position.y + (Mathf.Sin(rotateTime) * rotHeight);
        float z = rotPos.position.z;

        transform.localPosition = startPos + new Vector3(x, y, z);
    }

    public float DirtMeterCalculation()
    {
        return dirtTime / maxDirtTime;
    }

    public void CleanRag()
    {
        if (Input.GetKey(KeyCode.R))
        {
            ragRenderer.enabled = false;
            dirtTime -= 1.5f * Time.deltaTime;
        }
        else
        {
            ragRenderer.enabled = true;
        }
    }


}
