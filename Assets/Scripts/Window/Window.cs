﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    private RagMotion ragMotion;
    public GameObject Rag;

    public int cleanDuration;
    public float cleanTimer = 0;
    public float cleanTime;
    public bool isCleaning = false;
    public bool isReady = false;

    public bool canClean = false;
    // Use this for initialization
    void Start ()
    {
        ragMotion = Rag.GetComponent<RagMotion>();
        ragMotion.currentWindow = gameObject;
        cleanTime = Random.Range(3, 8);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButton(0) && !canClean)
        {
            isCleaning = false;
            Debug.Log("Practice");

        }
        else if (Input.GetMouseButton(0) && canClean)
        {
            isCleaning = true;
            Debug.Log("RealShit");
        }
        else
        {
            isCleaning = false;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        //currentWindow = other.gameObject;
        if (other.gameObject.tag == "Rag")
        {
            
            ragMotion.currentWindow = gameObject;
            canClean = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Rag")
        {
            isCleaning = false;
            canClean = false;
        }
    }
}
