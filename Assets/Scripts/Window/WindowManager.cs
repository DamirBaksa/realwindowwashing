﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager: MonoBehaviour {

    public List<Window> windows = new List<Window>();
    public Material[] dirt;
    public MeshRenderer winRend;
    private RagMotion ragScr;

    public Text winRemaining;
    public GameObject currentWindow, Rag;
    public Color dirtColor, clearColor;

    private int index = 0;
    public float switchSpeed = 1.0f;
    private float t = 0;
    private int windowsRemaining;
    // Use this for initialization
    void Start ()
    {
        windowsRemaining = 20;
        ragScr = Rag.GetComponent<RagMotion>();
        //for (int i = 0; i < windows.Count - 1; i++)
        foreach (Window win in windows)
        {
           // windows.Add(win);
            MeshRenderer winrend = win.GetComponent<MeshRenderer>();
            winrend.material = dirt[(Random.Range(0, dirt.Length - 1))];
            win.cleanDuration = Random.Range(5, 20);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        winRemaining.text = "Windows Remaining: " + windowsRemaining + "/20"; 

        // for (int i = 0; i < windows.Length - 1; i++)

      // for (int win = 0; win < windows.Length - 1; win++)
        foreach (Window win in windows)
        {
            if (win.isCleaning)
            {
                currentWindow = win.gameObject;
                Renderer cleanRend = currentWindow.GetComponent<Renderer>();
                Window window = currentWindow.GetComponent<Window>();
                cleanRend.material.color = Color.Lerp(dirtColor, clearColor, t);
                Debug.Log(currentWindow);
                if (t <= 1)
                {
                    t += Time.deltaTime / window.cleanDuration;
                    if (t >= 1)
                    {
                        cleanRend.material.color = clearColor;
                        //window.isCleaning = false;
                        windows.Remove(win);
                        t = 0;
                        windowsRemaining--;
                        Debug.Log(windowsRemaining);
                    }
                    else if (ragScr.dirtTime >= 7)
                    {
                        t -= Time.deltaTime / window.cleanDuration;
                       // cleanRend.material.color = Color.Lerp(clearColor, dirtColor, t);
                    }

                }
            }
        }
	}
}
