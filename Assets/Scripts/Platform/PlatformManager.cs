﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject ControlPanel;
    public GameObject controlText;
    public Text activationText;

    private PlatformMovement platformMovement;
    private PlayerControl playerControl;

    public bool canStart = false;
    public bool canStop = false;

	// Use this for initialization
	void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (canStart && Input.GetKeyDown(KeyCode.E))
        {
            activationText.text = "Press E to DeActivate";
            PlatFormMovement();
            canStart = false;
            Debug.Log("Platform Movement Enabled");
        }
        else if (!canStart && Input.GetKeyDown(KeyCode.E))
        {
            activationText.text = "Press E to Activate";
            PlayerMovement();
            canStart = true;
            Debug.Log("Platform Movement Disabled");
        }
    }

    private void PlatFormMovement() // Call to incorporate platform movement 
    {
        // controlText.SetActive(true);
        // Setting player movement to match platform movement
        Player.GetComponent<PlayerControl>().canMove = false; 
        gameObject.GetComponent<PlatformMovement>().enabled = true; // enabling Platform movement
    }

    private void PlayerMovement() // Call to bring back reg player movement
    {
       // controlText.SetActive(false);
        // Setting player movement to regular movement
        Player.GetComponent<PlayerControl>().canMove = true; 
        gameObject.GetComponent<PlatformMovement>().enabled = false; // disabling Platform movement
    }
    
    //Function to activate platform accesibility
    public void ActivatePlatform()
    {
        canStart = true;
        activationText.text = "Press E to Activate";
        ControlPanel.SetActive(true);
    }
    //Function to deactivate platform accesibilty
    public void DeactivatePlatform()
    {
        canStart = false;
        ControlPanel.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ActivatePlatform();
            other.gameObject.transform.parent = transform; // setting playerobj as child to platform to inherent platform movement
            Debug.Log("CanActivate");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.transform.parent = other.transform;
            DeactivatePlatform();
            other.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
