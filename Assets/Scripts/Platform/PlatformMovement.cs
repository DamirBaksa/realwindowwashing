﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    public float vertSpeed;
    public float horSpeed;
    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Basic platform Movement
        Vector3 tDir = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * vertSpeed);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(- Vector3.up * vertSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.forward * horSpeed);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(- Vector3.forward * horSpeed);
        }
    }
}
