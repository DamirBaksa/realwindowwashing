﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    public Transform playerCamera;
    public GameObject Rag;
    public GameObject ragControlObject;

    private MovementController movementController;
    public Text elevationText;

    public bool canDisable = false;
    public bool canMove = true;
    public bool canLook = true;
    public bool isEquipped = false;
	// Use this for initialization
	void Start ()
    {
        Rag.SetActive(false);
        movementController = GetComponent<MovementController>();
        movementController.moveSpeed = 10;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        // Basic player movement with flag to turn off
        if (canMove)
        {
            ragControlObject.SetActive(true);
            RagEquip();
            Vector3 tDir = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
            {
                tDir = transform.forward;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                tDir = -transform.forward;
            }
            if (Input.GetKey(KeyCode.D))
            {
                tDir += transform.right;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                tDir += -transform.right;
            }
            tDir.y = 0.0f;
            tDir.Normalize();
            movementController.SetDirection(tDir);
        }
        else
        {
            ragControlObject.SetActive(false);
        }
        // basic camera worx, with flag to turn off
        if (canLook)
        {
            Vector3 mouseDelta = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f);
            mouseDelta.Normalize();

            transform.Rotate(0.0f, mouseDelta.y, 0.0f, Space.World);
            playerCamera.Rotate(mouseDelta.x, 0.0f, 0.0f, Space.Self);
        }
        elevationText.text = "Elevation:" + transform.position.y;

    }

    public void RagEquip()
    {
        if (!isEquipped && Input.GetKey(KeyCode.Q))
        {
            Rag.SetActive(true);
            isEquipped = true;
        }
        else if (isEquipped && Input.GetKey(KeyCode.Q))
        {
            Rag.SetActive(false);
            isEquipped = false;
        }
        
    }
}

